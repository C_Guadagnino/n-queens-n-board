# N - Queens
 #### Operation :
- To Run this program, change the two parameters inside of `n_queens(board_size, current_column)` at the bottom of the function. Feel free to choose any number, it will return the number of solutions as well as show you the boards with the solutions if you so please. 
    - A side note : The number of solutions get's large FAST so please be kind to your computer.

- #### The Process :
	- we have to first check the board to see if the location we're looking at is "in danger"
	- if this is **True** then we must move on to the next check
	- if this is **False** then we change the value at that index to 1 to represent the queen

#### Learning : 
- Though this approach seemed simple at first I learned quite a lot about recursion
	- Two things that I learned :
		1. However deep you go recursively you must return from it, so you have to be able to have your board be *changeable* throughout the recursive looping
		2. I learned what a deep copy and a shallow copy are, also how to use the copy library. I believe this is probably not memory efficient, but it was interesting to learn and something that will benefit me in the future
- Outside of recursion I learned : 
	- Different ways to print data, such as looping through a list then printing each item (neat!)
	- Making a constructor function, doing most of my problem work in Leetcode has the detriment of never having me create a constructor, so this was a good learning experience for me! 

- #### PRINT A LOT :
	- I printed a lot of data, it's mostly been commented out, but it was extremely useful in diagnosing what was doing what.
	- 
- #### DRAW A LOT : 
	- I went through a lot of digital paper drawing diagrams and trying to understand how the process was looping. It was too hard to keep everything in my head, so using another app to draw everything was a live saver.
	

##### Special Thanks to Curtis for reviewing this code! 